package it.unibo.oop.lab.mvc;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class SimpleController implements Controller{
    
    private String nextString;
    private List<String> stringHistory = new LinkedList<>();

    @Override
    public void setNextStringToPrint(String nextString) {
        this.nextString=Objects.requireNonNull(nextString, "This method does not accept null values.");
    }

    @Override
    public String getNextStringToPrint() {
        return nextString;
    }

    @Override
    public List<String> getPrintedStringHistory() {
        return this.stringHistory;
    }

    @Override
    public void printCurrentString() {
        if (this.nextString == null) {
            throw new IllegalStateException("There is no string set");
        }
        this.stringHistory.add(this.nextString);
        System.out.println(this.nextString);
    }

}
